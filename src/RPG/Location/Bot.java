package RPG.Location;

public class Bot {
    private int x;
    private int y;

    public Bot(int a, int b) {
        x = a;
        y = b;
    }

    public void standHere() {
        Location.map[x][y] = "X ";
    }
}
