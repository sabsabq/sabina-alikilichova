package RPG.Location;

public class Gamer {
     int x;
     int y;



     public Gamer(int a, int b){
         x=a;
         y=b;
     }

    public void standHere() {
        Location.map[x][y] = "O ";
    }

    public void stepUp(){
         if (Location.map[x-1][y].equals("X ")){
             Location.map[x-1][y] = "V ";
             Location.map[x][y] = "* ";
             x-=1;
         }
         else{
             Location.map[x-1][y] = "O ";
             Location.map[x][y] = "* ";
             x-=1;
         }

    }

    public void stepDown(){
         if (Location.map[x+1][y].equals("X ")){
             Location.map[x+1][y] = "V ";
             Location.map[x][y] = "* ";
             x+=1;
         }
         else{
             Location.map[x+1][y] = "O ";
             Location.map[x][y] = "* ";
             x+=1;
         }

    }

    public void stepRight(){
         if (Location.map[x][y+1].equals("X ")){
             Location.map[x][y+1] = "V ";
             Location.map[x][y] = "* ";
             y +=1;
         }
         else{
             Location.map[x][y+1] = "O ";
             Location.map[x][y] = "* ";
             y += 1;
         }

    }

    public void stepLeft(){
         if ( Location.map[x][y-1].equals("X ")){
             Location.map[x][y - 1] = "V ";
             Location.map[x][y] = "* ";
             y-=1;
         }
         else{
             Location.map[x][y - 1] = "O ";
             Location.map[x][y] = "* ";
             y -= 1;
         }
    }

}
