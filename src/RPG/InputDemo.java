package RPG;

import java.util.Scanner;

public class InputDemo {
    public static void main(String[] args) {
        String ch;
        boolean run = true;
        while (run){
            Scanner in = new Scanner (System.in);
            ch = in.nextLine();
            run = !ch.equals("q");
            if (run) {
                System.out.println("Input: " + ch.toUpperCase());
            }
        }
        System.out.println("FINISHED");
    }
}
