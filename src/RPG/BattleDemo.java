package RPG;

public class BattleDemo {
    public static void main(String[] args) {

        Character char1 = new Character("Ivan");
        Character char2 = new Character("Artem");
        while ((char1.isAlive())&&(!char2.isDead())){
            char1.setHp(char1.getHp() - char2.getHit());
            System.out.println(char1.getHp());
            if(char1.isDead()) break;
            char2.setHp(char2.getHp() - char1.getHit());
            System.out.println(char2.getHp());
            System.out.println();
        }
        if (char1.isDead()){
            System.out.println(char1 + " is dead.");
        }
        else {
            System.out.println(char2 + " is dead.");
        }
    }
}
