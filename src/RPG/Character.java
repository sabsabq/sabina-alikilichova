package RPG;

import java.util.concurrent.ThreadLocalRandom;

public class Character {
    private int lvl = 1;
    private double hp = 100;
    private double strength = 10;
    private String name = "Unknown";

    @Override
    public String toString() {
        return name;
    }

    public double getHp() {
        return hp;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    public Character(String name) {
        this.name = name;
    }

    public double getMinHit() {
        double j = strength * 0.5 + lvl;
        return j;
    }

    public double getMaxHit() {
        double i = strength * 1 + lvl;
        return i;
    }

    public int getHit() {
        double f = Math.random();
        int min = (int) Math.round(getMinHit());
        int max = (int) Math.round(getMaxHit());
        int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
        return randomNum;
    }

    public boolean isAlive() {
        return hp > 0;
    }

    public boolean isDead() {
        return hp <= 0;
    }

    protected Artifact artifacts[] = new Artifact[5];
    public void addArtifact (Artifact a){

    }





}
