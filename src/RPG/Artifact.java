package RPG;

public class Artifact {
    private String types[] = {"weapon", "helmet", "chestplate", "boots", "shield"};
    private String type;
    private int hp;
    private int strenght;
    private int armor;


    public Artifact(String type, int hp, int strenght, int armor) {
        boolean found = false;
        for (String type1 : types) {
            if (type.equals(type1)) {
                found = true;
            }
        }
        if (!found) {
            throw new RuntimeException("Wrong artifact type " + type);
        }
    }

    public String getType() {
        return type;
    }

    public int getHp() {
        return hp;
    }

    public int getStrenght() {
        return strenght;
    }

    public int getArmor() {
        return armor;
    }
}
