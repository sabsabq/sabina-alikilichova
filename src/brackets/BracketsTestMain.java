package brackets;

import state.State;

import java.util.Scanner;

public class BracketsTestMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        boolean b = areBracketsCorrect(s);
        System.out.println(b);
    }

    static boolean areBracketsCorrect(String str) {
        Stack stack = new Stack(100);
        char[] symbols = str.toCharArray();
        for (char c : symbols) {
            if (c == '(' || c == '[' || c == '{') {
                stack.push(c);
            } else {
                if (c == ')' || c == '}' || c == ']') {
                    char r = stack.pop();
                    if (r == '(' && c == ')' || r == '{' && c == '}' || r == '[' && c == ']') {
                        return true;
                    }

                    else{
                        return false;
                    }
                }
            }
        }
        return true;
    }
}

