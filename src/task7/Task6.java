package task7;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        String firstWord;
        String secondWord;

        Scanner in = new Scanner(System.in);
        System.out.print("Введите первое слово: ");
        firstWord = in.nextLine();
        System.out.print("Введите второе слово: ");
        secondWord = in.nextLine();
        char[] arr1;
        arr1 = secondWord.toCharArray();
        Arrays.sort(arr1);
        char[] arr2;
        arr2 = firstWord.toCharArray();
        Arrays.sort(arr2);
        boolean flag = true;
        if (firstWord.length()== secondWord.length()){
            for(int i = 0; i < arr2.length && flag; i++) {
                if(arr1[i] != arr2[i]) {
                    flag = false;
                    break;
                }
            }
        }
        else flag = false;
        System.out.println(flag);
    }
}
