package task7;

import java.text.DecimalFormat;

public class Task3 {
    public static void main(String[] args) {
        float sum = 0;
        for (int i =0; i<args.length; i++){
            sum += Float.parseFloat(args[i]);
        }

        System.out.printf("%.2f", sum);
    }
}
