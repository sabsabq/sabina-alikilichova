package task8;

public class DeskDemo {
    public static void main(String[] args) {
        Desk d = new Desk();
        Class cl1= d.getClass();
        Color col = (Color)cl1.getAnnotation(Color.class);


        System.out.println("Desk color is: " + col.value());

    }
}
