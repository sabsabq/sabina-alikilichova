import java.util.Scanner;

public class TestDemo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int areYouReady;
        int count = 0;
        int answer;
        int correctStringIndex = 0;
        String answerString;
        String[] questions = {
                "Сколько ног у здорового человека?",
                "Какое имя у президента Татарстана?",
                "Какой сейчас год?",
                "Назовите столицу США",
                "Сколько мест в стандартной комнате в Деревне Универсиады?"
        };
        String[][] answers = {
                {"1", "*2", "3"},
                {"*Рустам", "Рустем", "Руслан"},
                {"2017", "*2018", "2019"},
                {"Нью-Йорк", "Бостон", "*Вашингтон"},
                {"2", "3", "*4"}
        };

        System.out.println("Напишите мне 1, если готовы начать!");
        areYouReady = in.nextInt();
        if (areYouReady != 1) {
            System.out.println("Жаль, приходите снова!");
        }
        else {
            for (int k = 0; k < 5; k++) {
                System.out.println((k + 1) + ". " + questions[k]);
                for (int i = 0; i < 3; i++) {
                    answerString = answers[k][i];
                    if (answerString.charAt(0) == '*') {
                        answerString = answerString.substring(1);
                        correctStringIndex = i;
                    }
                    System.out.print((i+1) + ") " + answerString + " ");
                    System.out.println();
                }
                System.out.println();
                System.out.print("Введите ответ цифрой: ");
                answer = in.nextInt();
                if (answer == correctStringIndex + 1) {
                    count++;
                }
            }

            System.out.println("Ваш результат: " + count + "/5");
            if (count == 5) {
                System.out.println("Красава от души!");
            }
        }

    }
}
