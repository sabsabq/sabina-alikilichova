package task12;



public class Boat implements Fuelable {
    private int fuel = 0;

    public int getFuel(){
        return fuel;
    }

    @Override
    public void setFuel(int i) {
        fuel = i;
    }
}
