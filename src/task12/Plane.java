package task12;

public class Plane implements Fuelable {
    private int fuel = 0;

    public int getFuel(){
        return fuel;
    }


    @Override
    public void setFuel(int i) {
        fuel = i;
    }
}
