package Homework01;

import java.util.Collections;

public class ArrayIntList implements IntList {
    private final static int INITIAL_CAPACITY = 10;
    private final static double COEFFICIENT = 1.5;
    private int[] arr;
    private int n;

    @Override
    public void add(int elem) {
        if (n == arr.length) {
            int[] newArray = new int[(int) (arr.length * COEFFICIENT)];
            for (int i = 0; i < arr.length; i++) {
                newArray[i] = arr[i];
            }
            arr = newArray;
        }
        arr[n++] = elem;
    }

    @Override
    public void add(int elem, int position) {
        if (n == arr.length) {
            int[] newArray = new int[(int) (arr.length * COEFFICIENT)];
            int k = position;

            for (int i = 0; i < k; i++){
                newArray[i] = arr[i];
            }
            newArray[k] = elem;
            for (int i = k; i < arr.length+1; i++) {
                newArray[i] = arr[i+1];
            }
            arr = newArray;
        }
    }

    @Override
    public int get(int index) {
        return arr[index];
    }

    @Override
    public int remove(int index) {
        if (n == arr.length) {
            int[] newArray = new int[(int) (arr.length * COEFFICIENT)];
            for (int i = 0; i < index; i++) {
                newArray[i] = arr[i];
            }
            for (int i = index; i < arr.length; i++) {
                newArray[i] = arr[i + 1];
            }
            arr = newArray;
        }
        return arr[index];
    }

    @Override
    public int size() {
        int k = (int)(arr.length*COEFFICIENT);
        return k;
    }

    @Override
    public int[] toArray() {
        return new int[0];
    }

    @Override
    public void sort() {
        if (n == arr.length) {
            int[] newArray = new int[(int) (arr.length * COEFFICIENT)];
            for (int i = 0; i < arr.length; i++) {
                newArray[i] = arr[i];
            }

        }


    }

    @Override
    public void addAll(IntList list, int position) {

    }

    @Override
    public int lastIndexOf(int elem) {
        return 0;
    }
}