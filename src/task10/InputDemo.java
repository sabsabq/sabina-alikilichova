package task10;

import java.util.Scanner;

public class InputDemo {
    public static void main(String[] args) {
        String ch;
        String name = null;
        String old;
        String mood;
        String end;

        boolean run = true;
        while (run) {
            System.out.print("Введите сообщение: ");
            Scanner in = new Scanner(System.in);
            ch = in.nextLine();

            if (ch.equals("Hello")) {
                System.out.println("Hi, what is your name?");
                Scanner nameIn = new Scanner(System.in);
                name = nameIn.nextLine();

                if (name.length() != 0) {
                    System.out.println("How old are you," + name + " ?");
                    Scanner oldIn = new Scanner(System.in);
                    old = oldIn.nextLine();
                    if (old.length() != 0) {
                        System.out.println("How are you?");
                        Scanner moodIn = new Scanner(System.in);
                        mood = moodIn.nextLine();
                        if (mood.equals("Ok")) {
                            System.out.println("Good");
                            Scanner endIn = new Scanner(System.in);
                            end = endIn.nextLine();
                            run = !end.equals("Any question?");
                            System.out.println("No. Bye, " + name);
                        } else {
                            if (mood.equals("Bad")) {
                                System.out.println("Sorry");
                                Scanner endIn = new Scanner(System.in);
                                end = endIn.nextLine();
                                run = !end.equals("Any question?");
                                System.out.println("No. Bye, " + name);
                            }
                        }
                    }
                }
            }
        }
    }
}
