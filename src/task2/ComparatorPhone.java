package task2;


import java.util.Comparator;

public class ComparatorPhone implements Comparator<Phone> {
    @Override
    public int compare(Phone o1, Phone o2) {
        if (o1.getPower() > o2.getPower()) {
            return 1;
        } else if (o1.getPower() < o2.getPower()) {
            return -1;
        } else {
            return 0;
        }
    }
}
