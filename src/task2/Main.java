package task2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    public static void mainPhone() {
        Comparator<Phone> pricePhone = new Comparator<Phone>() {
            @Override
            public int compare(Phone o1, Phone o2) {
                return Integer.compare(o1.getPrice(), o2.getPrice());
            }
        };

        ArrayList<Phone> phones = new ArrayList<>();
        phones.add(new Phone(1000, 5, "iPhone"));
        phones.add(new Phone(800, 7, "Samsung"));
        phones.add(new Phone(300, 7, "Nokia"));
        phones.add(new Phone(500, 6, "LG"));
        Collections.sort(phones, pricePhone);
        for (Phone l1 :
                phones) {
            System.out.println(l1.toString());
        }
        ComparatorPhone cm = new ComparatorPhone();
        for (Phone l1 :
                phones) {
            for (Phone l2 :
                    phones) {
                cm.compare(l1, l2);
            }
        }
        for (Phone l1 :
                phones) {
            System.out.println(l1.toString());
        }
    }
}
