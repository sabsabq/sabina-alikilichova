package task2;

public class Phone {
    private int price;
    private int power;
    private String name;

    public Phone(int price, int power, String name) {
        this.price = price;
        this.power = power;
        this.name = name;
    }

    public int getPrice() {
        return price;
    }
    public void setAge(int age) {
        this.price = price;
    }
    public int getPower() {
        return power;
    }
    public void setPower(int power) {
        this.power = power;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "age=" + price +
                ", power=" + power +
                ", name='" + name + '\'' +
                '}';
    }


}
