package homework02;

public class Queue {
    private int n;
    private int[] queue;
    private int maxSize;
    private int r = - 1;
    private int f = 0;

    public Queue(int maxSize) {
        this.maxSize = maxSize;
        queue = new int[maxSize];
    }
    public void enqueue(int elem) {
            if (r == maxSize - 1) {
                r = -1;
            }

            queue[++r] = elem;
            n++;
        }

    public int dequeue() {
        int temp = queue[f++];
        if (f == maxSize) {
            f = 0;
        }
        n--;
        return temp;


      }
    }

