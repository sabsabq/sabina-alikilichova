package homework02;

public class StackForLists {
    private int n;
  private Node third;

    void push(int c ){
        if (isEmpty()) {
            throw new IllegalStateException("Stack is full(((");
        }
        Node newNode = new Node();
        newNode.value = c;
        if (third != null) {
            Node current = third;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        } else {
            third = newNode;
        }
        n++;

    }
    int pop() {
        if (n==0) {
            throw new IllegalStateException("Stack is empty(((");
        }
        return --n;
    }
    boolean isEmpty(){
        return n ==0;
    }

}
class Node {
    int value ;
    Node next;

}
