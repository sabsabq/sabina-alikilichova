package _TASKS;

public class Task027 {
    public static void main(String[] args) {
        boolean k = false;
        int x;
        for (int i = 0; i< args.length; i++){
            x = Integer.parseInt(args[i]);
            if ((x % 6 == 0) || (x % 30 == 0)){
                k = true;
                break;
            }
        }
        if (k == true){
            System.out.println("Число найдено");
        }
        else {
            System.out.println("Число не найдено");
        }
    }
}
