package lastHomework;

public class RectangleParallepiped {
    private double lenght;
    private double width;
    private double higth;

    private double square;
    private double perimeter;
    private double volume;

    public RectangleParallepiped(double lenght, double width, double higth) {
        this.higth = higth;
        this.lenght = lenght;
        this.width = width;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }
    public double getLenght() {
        return lenght;

    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getWidth() {
        return width;
    }

    public void setHigth(double higth) {
        this.higth = higth;
    }
    public double getHigth() {
        return higth;
    }

    public double getPerimeter() {
        return 4*(higth + width + lenght);
    }

    public double getSquare() {
        return 2*( lenght*width + lenght*higth + width*higth );
    }

    public double getVolume() {
        return (lenght * width * higth);
    }
}

