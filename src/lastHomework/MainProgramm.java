package lastHomework;

public class MainProgramm {
    public static void main(String[] args) {
        Point originOne = new Point(23, 94);
        RectanglePoint rectOne = new RectanglePoint(originOne, 100, 200);
        RectanglePoint rectTwo = new RectanglePoint(50, 100);

        rectTwo.origin = originOne;
        rectOne.move(40, 72);
        System.out.println(rectTwo.origin.x);
        System.out.println(rectTwo.origin.y);
    }
}
