package lastHomework;

public class Point {
    public int x;
    public int y;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void change(int x1, int y1){
        x += x1;
        y += y1;
    }
}
