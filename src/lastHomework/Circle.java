package lastHomework;

public class Circle {
    private double radius;
    private double pi = 3.14;

    private double circuit;
    private double squar;

    public Circle(double radius) {
        this.radius = radius;
    }
    public void setRadius (double radius) {
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    public double getCircuit() {
        return (2 * pi * radius);
    }

    public double getSquar() {
        return (pi * radius * radius);
    }
}
