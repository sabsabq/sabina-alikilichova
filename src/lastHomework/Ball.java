package lastHomework;

public class Ball {
    private double radius;
    private double pi = 3.14;

    private double surfaceSqure;
    private double volume;

    public Ball (double radius) {
        this.radius = radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    public double getSurfaceSqure() {
        return (4 * pi * radius*radius);
    }
    public double getVolume() {
        return (4/3 * pi * radius*radius*radius);
    }
}
