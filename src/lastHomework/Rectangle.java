package lastHomework;

public class Rectangle {
    private double lenght;
    private double width;

    private double squar;
    private double perimeter;

    public Rectangle(double lenght, double width) {
        this.lenght = lenght;
        this.width = width;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }
    public double getLenght() {
        return lenght;
    }


    public void setWidth(double width) {
        this.width = width;
    }
    public double getWidth() {
        return width;
    }

    public double getSquar() {
        return 2*(lenght + width);
    }

    public double getPerimeter() {
        return (lenght*width);
    }
}
