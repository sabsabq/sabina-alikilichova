package homework03;


public  interface Map<K, V> {
    void put(K key, V value);
    V get(K key);
    void print();
}
