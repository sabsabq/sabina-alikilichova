package homework03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File f = new File("input");
        Scanner sc = new Scanner(f);
        String str;

        Map<String, Integer> m = new SimpleMap<>();


        while (sc.hasNext()) {
            str = sc.next();
            int n = 1;
            if (m.get(str) == (null)) {
                m.put(str, n);
            } else {
                m.put(str, m.get(str)+1);
            }
        }
        ((SimpleMap<String, Integer>) m).print();
    }
}
