package homework03;

public class SimpleMap<K, V> implements Map<K, V> {
    private static final int SIZE = 10;

    private Entry<K, V>[] entries;
    private int n;


    SimpleMap() {
        this.entries = new Entry[SIZE];
        this.n = 0;
    }

    @Override
    public void put(K key, V value) {
        if (n == entries.length) {
            Entry[] newArray = new Entry[(entries.length + 1)];
            for (int i = 0; i < entries.length; i++) {
                newArray[i] = entries[i];
            }
            entries = newArray;
        }
        for (int i = 0; i < n; i++) {
            if (entries[i].key.equals(key)) {
                entries[i].value = value;
                return;
            }
        }
        entries[n++] = new Entry<>(key, value);
    }

    //    @Override
//    public int get(K key) {
//        int c = 1;
//        for (int i = 0; i < n; i++) {
//            if (entries[i].key.equals(key)) {
//                c++;
//            }
//        }
//        return c;
//    }
    @Override
    public V get(K key) {
        for (int i = 0; i < n; i++) {
            if (entries[i].key.equals(key)) {
                return  entries[i].value;
            }
        }
        return null;
    }


    public void print() {
        for (int i = 0; i < entries.length; i++) {
            System.out.println("Key - " + entries[i].key + "   Value - " + entries[i].value);
        }
    }


    class Entry<K, V> {
        K key;
        V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
