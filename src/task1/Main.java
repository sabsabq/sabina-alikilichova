package task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Predicate;


public class Main {




    public static Map<String, Integer> uniqueFilter(Predicate p1) throws FileNotFoundException {
        Map<String, Integer> uniqueStrings = new LinkedHashMap<>();

        Scanner sc = new Scanner("words.txt");
        while (sc.hasNext()) {
            String current = sc.next();
            if (p1.test(current)) {
                if (uniqueStrings.containsKey(current)) {
                    uniqueStrings.put(current, uniqueStrings.get(current) + 1);
                } else uniqueStrings.put(current, 1);
            }
        }
        Map<String, Integer> uniqueSortedStrings = new LinkedHashMap<>();
        uniqueStrings.entrySet().stream().sorted(Comparator.comparing(t -> t.getValue())).forEach(t -> uniqueSortedStrings.put(t.getKey(), t.getValue()));
        return uniqueSortedStrings;

    }



}