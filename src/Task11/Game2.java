package Task11;


import java.util.Scanner;

public class Game2 {
    public static void main(String[] args) {
        boolean win = false;
        String xo[][] = {
                {"*", "*", "*"},
                {"*", "*", "*"},
                {"*", "*", "*"}
        };
        for (int i = 0; i < 3; i++) {
            for (int k = 0; k < 3; k++) {
                System.out.print(xo[k][i]);
            }
            System.out.println();
        }
        int m = 0;


        while (win == false) {
            Scanner in = new Scanner(System.in);
            System.out.print("PLAYER1: ");
            String player1 = in.nextLine();

            switch (player1) {
                case "a1":
                    xo[0][0] = "X";
                    m +=1;
                    break;

                case "a2":
                    xo[1][0] = "X";
                    m+=1;
                    break;

                case "a3":
                    xo[2][0] = "X";
                    m+=1;
                    break;

                case "b1":
                    xo[0][1] = "X";
                    m+=1;
                    break;

                case "b2":
                    xo[1][1] = "X";
                    m+=1;
                    break;

                case "b3":
                    xo[2][1] = "X";
                    m+=1;
                    break;

                case "c1":
                    xo[0][2] = "X";
                    m+=1;
                    break;

                case "c2":
                    xo[1][2] = "X";
                    m+=1;
                    break;

                case "c3":
                    xo[2][2] = "X";
                    m+=1;
                    break;
            }
            for (int i = 0; i < 3; i++) {
                for (int k = 0; k < 3; k++) {
                    System.out.print(xo[k][i]);
                }
                System.out.println();
            }
            if
                    (((xo[0][0] == "X") & (xo[0][1] == "X") & (xo[0][2] == "X")) ||
                    ((xo[1][0] == "X") & (xo[1][1] == "X") & (xo[1][2] == "X")) ||
                    ((xo[2][0] == "X") & (xo[2][1] == "X") & (xo[2][2] == "X")) ||
                    ((xo[0][0] == "X") & (xo[1][1] == "X") & (xo[2][2] == "X")) ||
                    ((xo[0][2] == "X") & (xo[1][1] == "X") & (xo[2][0] == "X")) ||
                    ((xo[0][0] == "X") & (xo[1][0] == "X") & (xo[2][0] == "X")) ||
                    ((xo[0][1] == "X") & (xo[1][1] == "X") & (xo[2][1] == "X")) ||
                    ((xo[0][2] == "X") & (xo[1][2] == "X") & (xo[2][2] == "X"))) {
                System.out.println("PLAYER1 WIN");
                win = true;
                break;
            }
            if ((win == false)&&(m==9)){
                System.out.println("DEAD HEAT");
                win = true;
                break;
            }
            System.out.print("PLAYER2: ");
            String player2 = in.nextLine();

            switch (player2) {
                case "a1":
                    xo[0][0] = "O";
                    m+=1;
                    break;

                case "a2":
                    xo[1][0] = "O";
                    m+=1;
                    break;

                case "a3":
                    xo[2][0] = "O";
                    m+=1;
                    break;

                case "b1":
                    xo[0][1] = "O";
                    m+=1;
                    break;

                case "b2":
                    xo[1][1] = "O";
                    m+=1;
                    break;

                case "b3":
                    xo[2][1] = "O";
                    m+=1;
                    break;

                case "c1":
                    xo[0][2] = "O";
                    m+=1;
                    break;

                case "c2":
                    xo[1][2] = "O";
                    m+=1;
                    break;

                case "c3":
                    xo[2][2] = "O";
                    m+=1;
                    break;
            }
            for (int i = 0; i < 3; i++) {
                for (int k = 0; k < 3; k++) {
                    System.out.print(xo[k][i]);
                }
                System.out.println();
            }


            if
                    (((xo[0][0] == "O") & (xo[0][1] == "O") & (xo[0][2] == "O")) ||
                    ((xo[1][0] == "O") & (xo[1][1] == "O") & (xo[1][2] == "O")) ||
                    ((xo[2][0] == "O") & (xo[2][1] == "O") & (xo[2][2] == "O")) ||
                    ((xo[0][0] == "O") & (xo[1][1] == "O") & (xo[2][2] == "O")) ||
                    ((xo[0][2] == "O") & (xo[1][1] == "O") & (xo[2][0] == "O")) ||
                    ((xo[0][0] == "O") & (xo[1][0] == "O") & (xo[2][0] == "O")) ||
                    ((xo[0][1] == "O") & (xo[1][1] == "O") & (xo[2][1] == "O")) ||
                    ((xo[0][2] == "O") & (xo[1][2] == "O") & (xo[2][2] == "O"))) {
                System.out.println("PLAYER2 WIN");
                win = true;
                break;
            }

        }

    }
}