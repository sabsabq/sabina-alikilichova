package Task11;

public class ArrayMathDemo {
    public static void main(String[] args) {
        int [] a  ={5, 7, 8, 10, 15, 2, 3};
        System.out.println(sum(a));
        System.out.println(avg(a));
        System.out.println(max(a));
        System.out.println(min(a));

    }
        public static int sum (int [] a){
            int sum = 0;
            for (int i = 0; i < a.length; i++){
                sum += a[i];
            }
            return sum;
        }

        public static double avg (int [] a){
            double avg = 0;
            for ( int i = 0; i < a.length; i++){
                avg += a[i];
            }
            avg = avg/a.length;
            return avg;
        }

        public static int max (int[]a){
            int max = a[0];
            for (int i = 1; i<a.length; i++){
                if (max < a[i]){
                    max = a[i];
                }
            }
            return max;
        }

        public static int min (int[]a){
            int min = a[0];
            for (int i = 1; i<a.length; i++){
                if (min > a[i]){
                    min = a[i];
                }
            }
            return min;
        }

}
